package main.java.datastructures;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Dequeue {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        List<Integer> arraySizeList = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Integer::parseInt)
                .collect(toList());
        List<Integer> array = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Integer::parseInt)
                .collect(toList());

        Deque<Integer> deque = new ArrayDeque<>(array.subList(0, arraySizeList.get(1)));
        Set<Integer> set = new HashSet<>(deque);
        int maxUniqueNumber = set.size();

        if(arraySizeList.get(0) > arraySizeList.get(1)){
            for (int i = 1; i <= arraySizeList.get(0) - arraySizeList.get(1); i++) {
                if(maxUniqueNumber < set.size()){
                    maxUniqueNumber = set.size();
                }
                if(maxUniqueNumber == arraySizeList.get(1)){
                    break;
                }else {
                    int first = deque.removeFirst();
                    if (!deque.contains(first)) {
                        set.remove(first);
                    }
                    int newLast = array.get(i + arraySizeList.get(1) -1);
                    deque.addLast(newLast);
                    set.add(newLast);
                }
            }
        }
        System.out.println(maxUniqueNumber);
        bufferedReader.close();
    }
}
