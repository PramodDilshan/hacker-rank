package main.problemsolving;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class CountApplesAndOranges {

    public static void countApplesAndOranges(int s, int t, int a, int b, List<Integer> apples, List<Integer> oranges) {
        int appleCount = 0;
        int orangeCount = 0;
        int i = 0;

        while (i < apples.size() || i < oranges.size()){
            if(i < apples.size()){
                if(apples.get(i) >=0 && (a + apples.get(i)) >= s && (a + apples.get(i) <= t)){
                    appleCount++;
                }
            }
            if(i < oranges.size()){
                if(oranges.get(i) <=0 && (b + oranges.get(i)) >= s && (b + oranges.get(i) <= t)){

                    orangeCount++;
                }
            }
            i++;

        }
        System.out.println(appleCount);
        System.out.println(orangeCount);

    }

}
