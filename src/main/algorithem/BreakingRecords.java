package main.algorithem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class BreakingRecords {

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(bufferedReader.readLine().trim());

        List<Integer> score = new ArrayList<>();
        score.addAll(Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Integer::parseInt)
                .collect(toList()));

        List<Integer> result = breakingRecords(score);

        System.out.printf("%d %d", result.get(0), result.get(1));



    }

    public static List<Integer> breakingRecords(List<Integer> scores) {
        List<Integer> high = new ArrayList<>();
        high.add(0, scores.get(0));
        high.add(1, 0);

        List<Integer> low = new ArrayList<>();
        low.add(0, scores.get(0));
        low.add(1, 0);

        for (int i = 1; i < scores.size(); i++) {
            if(scores.get(i) > high.get(0)){
                high.set(0, scores.get(i));
                high.set(1, high.get(1) + 1);
            }
            if(scores.get(i) < low.get(0)){
                low.set(0, scores.get(i));
                low.set(1, low.get(1) + 1);
            }
        }

        List<Integer> result = new ArrayList<>();
        result.add(0, high.get(1));
        result.add(1, low.get(1));

        return result;



    }
}
