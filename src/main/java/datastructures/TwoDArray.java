package main.java.datastructures;

import java.util.List;

public class TwoDArray {

    public static void maxSumHourGlass(List<List<Integer>> twoDArray){
        int maxSum = -63;
        for(int i = 1; i < twoDArray.size()-1 ; i++){
            for(int j = 1; j < twoDArray.get(0).size()-1; j++){
                int sum = twoDArray.get(i-1).get(j-1) +
                        twoDArray.get(i-1).get(j) +
                        twoDArray.get(i-1).get(j+1) +
                        twoDArray.get(i).get(j) +
                        twoDArray.get(i+1).get(j-1) +
                        twoDArray.get(i+1).get(j) +
                        twoDArray.get(i+1).get(j+1);
                maxSum = Math.max(maxSum, sum);

            }
        }
        System.out.println(maxSum);

    }


}
