package main.java.datastructures;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class SubArray {

    public void numberOfNegativeSubArrays() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(bufferedReader.readLine().trim());

        List<Integer> list = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Integer::parseInt).toList();

        int sumOfNegative = 0;

        for (int i = 0; i < list.size(); i++) {
            if(list.get(i) < 0){
                sumOfNegative++;
            }
            int sum = list.get(i);
            for (int j = i - 1; j >=0 ; j--) {
                sum += list.get(j);
                if(sum < 0){
                    sumOfNegative++;
                }
            }
        }
        System.out.println(sumOfNegative);
        bufferedReader.close();

    }
}
