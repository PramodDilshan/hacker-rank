package main.java.oop;
import java.io.*;


public class OverridingQ {


    public static void main(String[] args) throws IOException {

        Sports sports = new Sports();
        System.out.println(sports.getName());
        sports.getNumberOfTeamMembers();

        Sports soccer = new Soccer();
        System.out.println(soccer.getName());
        soccer.getNumberOfTeamMembers();



    }
}


class Sports{
    String getName(){
        return "Generic Sports";
    }
    void getNumberOfTeamMembers(){
        System.out.println( "Each team has n players in " + getName() );
    }
}

class Soccer extends Sports{
    @Override
    String getName(){
        return "Soccer Class";
    }

    @Override
    void getNumberOfTeamMembers(){
        System.out.println( "Each team has 11 players in " + getName() );
    }
}
