package main.java.oop;

import java.io.IOException;
import java.util.Scanner;

public class AbstractClass {

    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);

        Book myBook = new MyBook();
        myBook.setTitle(scanner.nextLine());
        System.out.printf("The title is: %s\n", myBook.getTitle());


    }
}

abstract class Book{
    String title;
    abstract void setTitle(String s);
    String getTitle(){
        return title;
    }
}

class MyBook extends Book{

    @Override
    void setTitle(String s) {
        this.title = s;
    }
}
