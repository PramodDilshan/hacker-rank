package main.problemsolving;

public class TimeConversion {

    public static String timeConversion(String s) {
        boolean isAM = s.substring(8,10).equalsIgnoreCase("AM");

        if(isAM){
            if(s.substring(0,2).equalsIgnoreCase("12")){
                s = s.replaceFirst("12", "00");
            }
        }else{
            if(!s.substring(0,2).equalsIgnoreCase("12")){
                s = s.replaceFirst("[0-9]{2}", String.valueOf(Integer.parseInt(s.substring(0,2))+12));
            }
        }
        return s.substring(0,8);



    }

}
