package main.algorithem;

public class FrogJump {
    public int solution(int x, int y, int d) {
        return (y-x)%d == 0 ? (y-x)/d : (y-x)/d+1;
    }

    public int solution(int[] skips, int end, int modulo){
        int[] dp = new int[end + 1];
        dp[0] = 1;
        for (int j = 1; j < end + 1; j++) {
            for (int i = 0; i < skips.length; i++) {
                if(skips[i] <= j){
                    dp[j] = (dp[j] + dp[j-skips[i]]);
                }
            }
        }
        return dp[end];
    }
}
