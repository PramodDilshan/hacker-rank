package main.problemsolving;

import java.util.List;

public class DiagonalDifference {
    public static int diagonalDifference(List<List<Integer>> arr) {
        int diff = 0;

        for (int x = 0; x < arr.size(); x++){
            diff += (arr.get(x).get(x) - arr.get(x).get(arr.size() - 1 - x));
        }


        return Math.abs(diff);

    }

}
