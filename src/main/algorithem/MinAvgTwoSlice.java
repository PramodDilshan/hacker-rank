package main.algorithem;

public class MinAvgTwoSlice {

    public int solution(int[] array){

        double minAvg = (array[0] + array[1]) / 2.0; // Initialize with the average of the first slice of length 2
        int startIndex = 0;

        for (int i = 0; i < array.length - 2; i++) {
            // Calculate average of slices of length 2 and 3 starting at index i
            double avg2 = (array[i] + array[i + 1]) / 2.0;
            double avg3 = (array[i] + array[i + 1] + array[i + 2]) / 3.0;

            // Update minAvg and startIndex if a new minimum average is found
            if (avg2 < minAvg) {
                minAvg = avg2;
                startIndex = i;
            }
            if (avg3 < minAvg) {
                minAvg = avg3;
                startIndex = i;
            }
        }

        // Check the last slice of length 2
        double lastAvg2 = (array[array.length - 2] + array[array.length - 1]) / 2.0;
        if (lastAvg2 < minAvg) {
            startIndex = array.length - 2;
        }

        return startIndex;
    }
}
