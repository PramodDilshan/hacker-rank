package main.java.introduction;

import java.util.Scanner;

public class StaticInitialization {
    public static int b;
    public static int h;
    static {
        Scanner scanner = new Scanner(System.in);
        b = scanner.nextInt();
        h = scanner.nextInt();
    }

    public static void main(String[] args){

        if(b <= 0 || h <= 0){
            System.out.println("java.lang.Exception: Breadth and height must be positive");
        }else {
            System.out.println(b*h);
        }
    }
}
