package main.java.datastructures;

public class Generic {
    public static void main(String[] args){
        Integer[] intArray = {1,2,3};
        String[] strArray = {"Hello", "World"};
        printGeneric(intArray);
        printGeneric(strArray);
    }

    public static <E> void printGeneric(E[] array){
        for (E value: array
        ) {
            System.out.println(value);
        }
    }
}
