package main.algorithem;

public class OddOccurrencesInArray {

    public int solution(int[] a){
        int num = 0;
        for (int j : a) {
            num ^= j;
        }
        return num;
    }

}
