package main.algorithem;

public class PermMissingElem {

    public int solution(int[] a) {
        long expectedSum = ((long)(a.length + 1) * (a.length + 2)) / 2;
        long actualSum = 0;
        for (int j : a) {
            actualSum += j;
        }
        return (int)(expectedSum - actualSum);
    }
}
