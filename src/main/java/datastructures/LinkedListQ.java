package main.java.datastructures;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class LinkedListQ {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(bufferedReader.readLine().trim());

        List<Integer> list = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Integer::parseInt)
                .collect(toList());

        int q = Integer.parseInt(bufferedReader.readLine().trim());

        IntStream.range(0, q).forEach(i -> {
            try {
                String operation = bufferedReader.readLine().trim();
                List<Integer> operatingValues = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                        .map(Integer::parseInt)
                        .collect(toList());

                if(operation.equalsIgnoreCase("Insert")){
                    doInsertion(list, operatingValues.get(0), operatingValues.get(1));
                }
                if(operation.equalsIgnoreCase("Delete")){
                    doDeletion(list, operatingValues.get(0));
                }
            }
            catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });

        list.forEach(value -> System.out.printf("%d ", value));

        bufferedReader.close();
    }

    private static void doInsertion(List<Integer> list, int index, Integer value) {
        list.add(index, value);
    }

    private static void doDeletion(List<Integer> list, int index) {
        list.remove(index);
    }
}
