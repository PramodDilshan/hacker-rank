package main.problemsolving;
import java.util.List;

public class CompareTheTriplets {
    public static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
        Integer[] comparisonScore = {0, 0};

        for (int i = 0; i < 3; i++){
            if(a.get(i) > b.get(i)){
                comparisonScore[0]++;
            }
            if(a.get(i) < b.get(i)){
                comparisonScore[1]++;
            }
        }
        return List.of(comparisonScore);

    }
}
