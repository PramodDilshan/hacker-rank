package main.java.datastructures;

import java.io.*;
import java.util.List;
import java.util.stream.Stream;

public class OneDArray {
    public static void printArray() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(System.out));

        int n = Integer.parseInt(bufferedReader.readLine().trim());
        int[] a = new int[n];

        List<Integer> aList = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Integer::parseInt).toList();

        for(int i = 0; i<aList.size(); i++){
            a[i] = aList.stream().iterator().next();
            System.out.println(a[i]);
        }
    }
}
