package main.java.introduction;

import java.io.IOException;
import java.util.Scanner;

public class EOF {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        int lineNumber = 1;

        while (scanner.hasNextLine()){
            String input = scanner.nextLine();
            System.out.printf("%d %s\n", lineNumber++, input);

        }
    }
}
