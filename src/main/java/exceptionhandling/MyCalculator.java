package main.java.exceptionhandling;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class MyCalculator {

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNextLine()){
            List<Integer> list = Stream.of(scanner.nextLine().replaceAll("\\s+$", "").split(" "))
                    .map(Integer::parseInt)
                    .collect(toList());
            try {
                System.out.println(power(list.get(0), list.get(1)));
            } catch (Exception e) {
                System.out.println(e);
            }

        }
    }

    public static long power(int n, int p) throws Exception {
        if(n < 0 || p < 0){
            throw new Exception("n or p should not be negative.");
        }
        if(n == 0 && p == 0){
            throw new Exception("n and p should not be zero.");
        }
        return (long) Math.pow(n,p);
    }

}
