package main.algorithem;

import java.util.Arrays;

public class MinAbsSum {

    // O(N^2.M) ; M = max(Abs(array))
    public int solution(int[] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            array[i] = Math.abs(array[i]);
            sum += array[i];
        }
        int[] dp = new int[sum+1];
        dp[0]  = 1;
        for (int j = 0; j < array.length; j++) {
            for (int i = sum; i > -1; i--) {
                if(dp[i] == 1 && i + array[j] <= sum){
                    dp[i+array[j]] = 1;
                }

            }

        }
        int result = sum;
        for (int i = 0; i < sum/2 + 1; i++) {
            if(dp[i] == 1){
                result = Math.min(result, sum - 2 * i);
            }

        }
        return result;

    }

    // O(N.M^2) ; M = max(Abs(array))
    public int solutionOptimized(int[] array) {
        int max = 0;
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            array[i] = Math.abs(array[i]);
            max = Math.max(array[i], max);
            sum += array[i];
        }

        int count[] = new int[max + 1];
        for (int i = 0; i < array.length; i++) {
            count[array[i]] += 1;
        }

        int[] dp = new int[sum+1];
        Arrays.fill(dp, -1);
        dp[0] = 0;

        for (int a = 1; a < max + 1; a++) {
            if(count[a] > 0){
                for (int j = 0; j < sum + 1; j++) {
                    if(dp[j] >= 0){
                        dp[j] = count[a];
                    } else if (j >= a && dp[j-a] > 0) {
                        dp[j] = dp[j-a] - 1;
                    }
                }
            }
        }

        int result = sum;
        for (int i = 0; i < sum/2 + 1; i++) {
            if(dp[i] >= 0){
                result = Math.min(result, sum - 2 * i);
            }

        }
        return result;

    }
}
