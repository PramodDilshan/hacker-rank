package main.java.exceptionhandling;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class TryCatch {

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        int x = 0;
        int y = 0;
        int z = 0;

        try {
            x = scanner.nextInt();
            y = scanner.nextInt();
        } catch (InputMismatchException e){
            System.out.println("java.util.InputMismatchException");
            System.exit(0);
        }
        try {
            z = x/y;
        }  catch (ArithmeticException e){
            System.out.println(e);
            System.exit(0);
        }
        System.out.println(z);
    }
}
