package main.algorithem;

public class MaxCounter {
    public int[] solution(int n, int[] a) {
        int[] counters = new int[n];
        int maxCounter = 0;
        int lastMaxCounter = 0;

        for (int operation : a) {
            if (operation >= 1 && operation <= n) {
                counters[operation - 1] = Math.max(counters[operation - 1], lastMaxCounter);
                counters[operation - 1]++;

                if (counters[operation - 1] > maxCounter) {
                    maxCounter = counters[operation - 1];
                }
            } else if (operation == n + 1) {
                lastMaxCounter = maxCounter;
            }
        }

        for (int i = 0; i < counters.length; i++) {
            counters[i] = Math.max(counters[i], lastMaxCounter);
        }

        return counters;
    }

}
