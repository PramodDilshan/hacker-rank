package main.java.datastructures;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class SortList {

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(bufferedReader.readLine());

        List<Student> playerList = new ArrayList<>();

        IntStream.range(0,n).forEach(i -> {
            try {
                List<String> playerValues = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                        .collect(toList());
                playerList.add(new Student(
                        Integer.parseInt(playerValues.get(0)),
                        playerValues.get(1),
                        Double.parseDouble(playerValues.get(2))));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        playerList.stream()
                .sorted(Comparator.comparing(Student::getId))
                .sorted(Comparator.comparing(Student::getFirstName))
                .sorted(Comparator.comparing(Student::getCgpa).reversed())
                .forEach(student -> System.out.printf("%s \n", student.getFirstName()));

        bufferedReader.close();
    }

    public static class Student {
        private int id;
        private String firstName;
        private double cgpa;

        public Student(int id, String firstName, double cgpa) {
            this.id = id;
            this.firstName = firstName;
            this.cgpa = cgpa;
        }

        public int getId() {
            return id;
        }

        public String getFirstName() {
            return firstName;
        }

        public double getCgpa() {
            return cgpa;
        }
    }
}
