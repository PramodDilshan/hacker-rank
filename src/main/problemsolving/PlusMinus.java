package main.problemsolving;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;

public class PlusMinus {

    public static void plusMinus(List<Integer> arr) {
        int positiveCount = 0;
        int negativeCount = 0;
        int zeroCount = 0;

        for (int ele: arr
             ) {
            if(ele == 0) {
                zeroCount++;
            }
            else if(ele > 0) {
                positiveCount++;
            }
            else {
                negativeCount++;
            }


        }
        MathContext mc = new MathContext(6, RoundingMode.FLOOR);
        System.out.println(BigDecimal.valueOf(positiveCount).divide(BigDecimal.valueOf(arr.size()), mc));
        System.out.println(BigDecimal.valueOf(negativeCount).divide(BigDecimal.valueOf(arr.size()), mc));
        System.out.println(BigDecimal.valueOf(zeroCount).divide(BigDecimal.valueOf(arr.size()), mc));

    }
}
