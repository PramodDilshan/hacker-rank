package main.java.introduction;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class DataTypes {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);

        scanner.nextLine();
        int count = 0;

        Map<Integer, String> map = new HashMap<>();
        map.put(1, "* long\n");
        map.put(2, "* int\n* long\n");
        map.put(3, "* short\n* int\n* long\n");
        map.put(4, "* byte\n* short\n* int\n* long\n");

        while (scanner.hasNextLine()){
            String input = scanner.nextLine();
            count = 0;
            try {
                long l = Long.parseLong(input);
                count++;
                int in = Integer.parseInt(input);
                count++;
                short s = Short.parseShort(input);
                count++;
                byte b = Byte.parseByte(input);
                count++;
                if(count>0){
                    System.out.printf("%s can be fitted in:\n%s", input, map.get(count));
                }
            } catch (NumberFormatException exception){
                if(count == 0){
                    System.out.printf("%s can't be fitted anywhere.\n", input);
                }else {
                    System.out.printf("%s can be fitted in:\n%s", input, map.get(count));
                }
            }
        }
    }
}
