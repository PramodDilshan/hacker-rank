package main.java.oop;

import java.io.IOException;
import java.util.Scanner;

public class InterfaceQ {

    public static void main(String[] args) throws IOException {

        AdvancedArithmetic arithmetic = new MyCalculator();
        System.out.printf("I implemented: %s\n", arithmetic.getClass().getInterfaces()[0].getSimpleName());

        Scanner scanner = new Scanner(System.in);

        System.out.println(arithmetic.divisorSum(scanner.nextInt()));


    }
}

interface AdvancedArithmetic{
    int divisorSum(int n);
}

class MyCalculator implements  AdvancedArithmetic{

    @Override
    public int divisorSum(int n) {
        int sum = 0;
        for (int i = 1; i <= n; i++) {
            if(n % i == 0){
                sum += i;
            }
        }
        return sum;
    }
}
