package main.problemsolving;

import java.util.*;

public class MiniMaxSum {
    public static void miniMaxSum(List<Integer> arr) {
        Collections.sort(arr);
        long min = 0;
        long max = 0;

        for(int i = 0; i < arr.size() - 1; i++){
            min += arr.get(i);
            max += arr.get(i+1);
        }
        System.out.printf("%d %d%n", min, max);


    }

}
