package main.problemsolving;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class GradingStudents {

    public static List<Integer> gradingStudents(List<Integer> grades) {
        return grades.stream().map(grade -> {
            if(grade > 37 && 5*((grade/5) + 1) - grade < 3) {
                grade = 5 * ((grade / 5) + 1);
            }
            return grade;
        }).collect(Collectors.toList());
    }
}
