package main.java.datastructures;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;

public class StackQ {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        bufferedReader.lines().forEach(line -> {
            Stack<Character> stack = new Stack<>();
            char[] charArray = line.toCharArray();
            for (int i = 0; i < charArray.length; i++) {
                if(isOpeningParentheses(charArray[i])){
                    stack.push(charArray[i]);
                }
                if(isClosingParentheses(charArray[i])){
                    if(!stack.isEmpty() && getMatchingCharacter(stack.peek()) == charArray[i]){
                        stack.pop();
                    }else {
                        stack.push(charArray[i]);
                    }
                }
            }
            System.out.println(stack.isEmpty() ? "true" : "false");
        });
        bufferedReader.close();
    }


    public static boolean isOpeningParentheses(char character){
        return (character == '{' || character == '(' || character == '[');
    }


    public static boolean isClosingParentheses(char character){
        return (character == '}' || character == ')' || character == ']');
    }

    public static char getMatchingCharacter(char character){
        char matchCharacter = 'a';
        switch (character){
            case '{':
                matchCharacter = '}';
                break;
            case '(':
                matchCharacter =  ')';
                break;
            case '[':
                matchCharacter =  ']';
                break;
        }
        return matchCharacter;
    }
}
