package main.java.datastructures;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class PriorityQueueQ {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int q = Integer.parseInt(bufferedReader.readLine());
        PriorityQueue<Student> studentQueue = new PriorityQueue(Comparator
                .comparing(Student::getCgpa).reversed()
                .thenComparing(Student::getName)
                .thenComparing(Student::getId)
        );

        IntStream.range(0, q).forEach(i -> {
            try {
                List<String> eventValues = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                        .collect(toList());
                String event = eventValues.get(0);
                if(event.equalsIgnoreCase("ENTER")){
                    Student student = new Student(eventValues.get(1), Double.parseDouble(eventValues.get(2)), Integer.parseInt(eventValues.get(3)));
                    studentQueue.add(student);
                }
                if(event.equalsIgnoreCase("SERVED")){
                    studentQueue.poll();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        });

        if(studentQueue.isEmpty()){
            System.out.println("EMPTY");
        }else{
            while (!studentQueue.isEmpty()){
                System.out.println(studentQueue.poll().getName());
            }
        }


        bufferedReader.close();
    }

    public static class Student{
        private int id;
        private String name;
        private double cgpa;

        public Student(String name, double cgpa, int id) {
            this.id = id;
            this.name = name;
            this.cgpa = cgpa;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public double getCgpa() {
            return cgpa;
        }
    }
}
