package main.java.datastructures;

import main.Application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class ComparatorsQ {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(bufferedReader.readLine());

        List<Player> playerList = new ArrayList<>();

        IntStream.range(0,n).forEach(i -> {
            try {
                List<String> playerValues = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                        .collect(toList());
                playerList.add(new Player(playerValues.get(0), Integer.parseInt(playerValues.get(1))));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

//        playerList.stream()
//                .sorted(Comparator.comparing(Player::getName))
//                .sorted(Comparator.comparing(Player::getScore).reversed())
//                .forEach(player -> System.out.printf("%s %d\n", player.getName(), player.getScore()));

        Collections.sort(playerList, new Checker());
        playerList.stream().forEach(player -> System.out.printf("%s %d\n", player.getName(), player.getScore()));



        bufferedReader.close();
    }

    public static class Player {
        private String name;
        private int score;

        public Player(String name, int score) {
            this.name = name;
            this.score = score;
        }

        public String getName() {
            return name;
        }

        public int getScore() {
            return score;
        }
    }

    public static class Checker implements Comparator<Player> {
        @Override
        public int compare(Player player1, Player player2) {
            int scoreComparator = Integer.compare(player2.getScore(), player1.getScore());
            if(scoreComparator == 0){
                return player1.getName().compareTo(player2.getName());
            }else {
                return scoreComparator;
            }
        }
    }
}
