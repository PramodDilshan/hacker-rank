package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class NewYear {

    public void happy() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String input = bufferedReader.readLine();
        String output = "Happy New " + input;
        System.out.println(output);
        bufferedReader.close();
    }
}
