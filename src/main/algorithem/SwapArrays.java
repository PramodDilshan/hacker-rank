package main.algorithem;

import java.util.HashSet;
import java.util.Set;

public class SwapArrays {

    public boolean solution(int[] a, int[] b) {
        int sumA = 0;
        int sumB = 0;
        for (int i = 0; i < a.length; i++) {
            sumA += a[i];
            sumB += b[i];
        }

        if (sumA == sumB) {
            return true; // no swap needed
        }

        int diff = sumB - sumA;
        if (diff % 2 != 0) {
            return false; // cannot split odd difference equally
        }

        int target = diff / 2;
        Set<Integer> setB = new HashSet<>();
        for (int j : b) {
            setB.add(j);
        }

        for (int j : a) {
            int complement = target + j;
            if (setB.contains(complement)) {
                return true;
            }
        }

        return false; // no pair found to swap
    }
}
