package main.algorithem;

public class ArrayRotation {

    public int[] solution(int[] a, int k) {
        if(a.length == 0 || k == 0){
            return a;
        }
        int[] rotatedArray = new int[a.length];
        k = k % a.length;
        for (int i = 0; i < a.length; i++) {
            if(a.length > i + k){
                rotatedArray[i+k] = a[i];
            }else {
                rotatedArray[i + k - a.length] = a[i];
            }
        }
        return rotatedArray;
    }
}
