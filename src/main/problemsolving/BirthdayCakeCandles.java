package main.problemsolving;
import java.util.List;

public class BirthdayCakeCandles {
    public static int birthdayCakeCandles(List<Integer> candles) {
        int count = 0;
        int currentMax = 0;

        for (Integer candle: candles) {
            if(currentMax < candle){
                currentMax = candle;
                count = 1;
            }else if (currentMax == candle){
                count++;
            }
        }
        return count;
    }
}
