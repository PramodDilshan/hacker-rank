package main.problemsolving;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class MaximumOccurringCharacter {

    public static char maximumOccurringCharacter(String text) {

        Map<Character, Integer> countMap = new HashMap<>();
        for (char character: text.toCharArray()
        ) {
            if(countMap.containsKey(character)){
                countMap.put(character, countMap.get(character) + 1);

            }else {
                countMap.put(character, 1);
            }
        }

        Map.Entry<Character, Integer> maxEntry = null;

        for (Map.Entry<Character, Integer> entry : countMap.entrySet())
        {
            if (maxEntry == null || entry.getValue().compareTo(maxEntry.getValue()) > 0)
            {
                maxEntry = entry;
            }
        }

        return Objects.requireNonNull(maxEntry).getKey();
    }



}
