package main.algorithem;

public class BinaryGap {

    public int solution(int n) {
        int binaryGap = 0;
        int maxBinaryGap = 0;
        boolean validZero = false;
        String binary = Integer.toBinaryString(n);
        for (int i = 0; i < binary.length(); i++) {
            if(binary.charAt(i) == '0' && validZero){
                binaryGap++;
            }
            if(binary.charAt(i) == '1'){
                if(validZero){
                    maxBinaryGap = Math.max(maxBinaryGap, binaryGap);
                    binaryGap = 0;
                }else {
                    validZero = true;
                }
            }
        }
        return maxBinaryGap;
    }
}
