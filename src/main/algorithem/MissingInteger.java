package main.algorithem;

public class MissingInteger {
    public int solution(int[] a){
        int n = a.length;
        boolean[] found = new boolean[n+1];
        for (int j : a) {
            if (j > 0 && j <= n) {
                found[j] = true;
            }
        }
        for (int i = 1; i <= n; i++) {
            if (!found[i]) {
                return i;
            }
        }
        return n+1;
    }
}
