package main.algorithem;

public class PermCheck {
    public int solution(int[] a) {
        int n = a.length;
        boolean[] seen = new boolean[n+1]; // initialize a boolean array to keep track of seen numbers

        for (int val : a) {
            if (val < 1 || val > n || seen[val]) {
                return 0; // if the number is outside the range [1, N] or is a duplicate, return 0
            } else {
                seen[val] = true; // mark the number as seen
            }
        }
        return 1; // if all numbers are in the range [1, N] and are unique, return 1
    }
}
