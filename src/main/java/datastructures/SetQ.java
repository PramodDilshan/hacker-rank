package main.java.datastructures;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

public class SetQ {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(bufferedReader.readLine());

        Set<String> set = new HashSet<>();

        IntStream.range(0,n).forEach(i -> {
            try {
                String value = bufferedReader.readLine();
                if(!set.contains(i)){
                    set.add(value);
                }
                System.out.println(set.size());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        bufferedReader.close();
    }
}
