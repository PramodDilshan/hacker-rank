package main.java.datastructures;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class ArrayList {
    public void printPosition() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(bufferedReader.readLine().trim());

        List<List<Integer>> twoDArrayList = new java.util.ArrayList<>();

        IntStream.range(0, n).forEach(i -> {
            try {
                twoDArrayList.add(
                        Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                                .map(Integer::parseInt)
                                .collect(toList())
                );
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        int q = Integer.parseInt(bufferedReader.readLine().trim());

        IntStream.range(0, q).forEach(i -> {
            List<Integer> query = null;
            try {
                query = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                        .map(Integer::parseInt)
                        .collect(toList());
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                System.out.println(twoDArrayList.get(query.get(0)-1).get(query.get(1)));                ;
            } catch (IndexOutOfBoundsException ex) {
                System.out.println("ERROR!");
            }
        });
        bufferedReader.close();
    }
}
