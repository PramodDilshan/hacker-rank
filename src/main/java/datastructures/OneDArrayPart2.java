package main.java.datastructures;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.ArrayList;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class OneDArrayPart2 {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(bufferedReader.readLine().trim());

        List<List<Integer>> sizeLeap = new ArrayList<>();
        List<List<Integer>> arrayList = new ArrayList<>();

        IntStream.range(0, n).forEach(i -> {
            try {
                sizeLeap.add(
                        Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                                .map(Integer::parseInt)
                                .collect(toList())
                );
                arrayList.add(
                        Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                                .map(Integer::parseInt)
                                .collect(toList())
                );
                canWin(sizeLeap.get(i).get(1), (ArrayList<Integer>) arrayList.get(i));

            }
            catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });
        bufferedReader.close();
    }

    public static boolean canWin(int leap, ArrayList<Integer> game){
        BitSet visitedIndex = new BitSet(game.size());
        boolean canWin  = traverse(0, leap, game, visitedIndex);
        if(canWin){
            System.out.println("YES");
        }else {
            System.out.println("NO");
        }
        return canWin;
    }

    public static boolean isWin(int currentIndex, int leap, ArrayList<Integer> game){
        if(currentIndex >= game.size() - leap || currentIndex == game.size() - 1){
            return true;
        }else {
            return false;
        }
    }

    public static boolean traverse(int currentIndex, int leap, ArrayList<Integer> game, BitSet visitedIndex){
        boolean canWin = isWin(currentIndex, leap, game);
        if(!canWin && currentIndex > 0 && !visitedIndex.get(currentIndex - 1) && game.get(currentIndex - 1) == 0){
            visitedIndex.set(currentIndex - 1);
            canWin = traverse(currentIndex - 1, leap, game, visitedIndex);
        }
        if(!canWin && currentIndex < game.size() - 1 && !visitedIndex.get(currentIndex + 1) && game.get(currentIndex + 1) == 0){
            visitedIndex.set(currentIndex + 1);
            canWin = traverse(currentIndex + 1, leap, game, visitedIndex);
        }
        if(!canWin && currentIndex < game.size() - leap && !visitedIndex.get(currentIndex + leap) && game.get(currentIndex + leap) == 0){
            visitedIndex.set(currentIndex + leap);
            canWin = traverse(currentIndex + leap, leap, game, visitedIndex);
        }
        return canWin;
    }
}
