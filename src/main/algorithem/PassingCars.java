package main.algorithem;

public class PassingCars {
    public int solution(int[] a) {
        int numEastbound = 0;
        int numPairs = 0;

        for (int j : a) {
            if (j == 0) {
                numEastbound++;
            } else {
                numPairs += numEastbound;

                if (numPairs > 1000000000) {
                    return -1;
                }
            }
        }

        return numPairs;
    }
}
