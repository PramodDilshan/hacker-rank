package main.java.datastructures;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public class MapQ {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(bufferedReader.readLine());

        Map<String, String> phoneBook = new HashMap<>();

        IntStream.range(0,n).forEach(i -> {
            try {
                String name = bufferedReader.readLine();
                String phoneNumber = bufferedReader.readLine();
                phoneBook.put(name,phoneNumber);

            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        bufferedReader.lines().forEach(name -> {
            System.out.printf(phoneBook.containsKey(name) ? "%s=%s\n" : "Not found\n", name, phoneBook.get(name));
        });

        bufferedReader.close();

    }
}
