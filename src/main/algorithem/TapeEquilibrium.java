package main.algorithem;

public class TapeEquilibrium {
    public int solution(int[] a) {
        int sumLeft = a[0];
        int sumRight = 0;
        for (int i = 1; i < a.length; i++) {
            sumRight += a[i];
        }
        int minDiff = Math.abs(sumLeft - sumRight);
        for (int p = 1; p < a.length - 1; p++) {
            sumLeft += a[p];
            sumRight -= a[p];
            int diff = Math.abs(sumLeft - sumRight);
            if (diff < minDiff) {
                minDiff = diff;
            }
        }
        return minDiff;
    }
}
