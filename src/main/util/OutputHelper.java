package main.util;

import java.util.List;

public class OutputHelper {

    public static void printIntegerList(List<Integer> list){
        list.forEach(value -> System.out.printf("%d ", value));
    }
}
