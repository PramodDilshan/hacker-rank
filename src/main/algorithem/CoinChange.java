package main.algorithem;

import java.util.List;

public class CoinChange {

    public int[] solution(List<Integer> coins, int amount){
        int[][] dp = new int[coins.size() + 1][amount+1];
        for (int i = 0; i < coins.size() + 1; i++) {
            dp[i][0] = 0;
        }
        for (int j = 1; j < amount + 1; j++) {
            dp[0][j] = Integer.MAX_VALUE;

        }

        for (int i = 1; i < coins.size() + 1; i++) {
            for (int j = 1; j < coins.get(i-1); j++) {
                dp[i][j] = dp[i-1][j];
            }
            for (int j = coins.get(i-1); j < amount+1; j++) {
                dp[i][j] = Math.min(dp[i][j-coins.get(i-1)] + 1, dp[i-1][j]);
            }
        }
        return dp[coins.size()];
    }

    public Integer[] solutionSpaceOptimized(List<Integer> coins, int amount){
        Integer[] dp = new Integer[amount+1];
        for (int j = 0; j < amount + 1; j++) {
            dp[j] = Integer.MAX_VALUE;

        }
        dp[0] = 0;
        for (int i = 1; i < coins.size() + 1; i++) {
            for (int j = coins.get(i-1); j < amount+1; j++) {
                dp[j] = Math.min(dp[j-coins.get(i-1)] + 1, dp[j]);
            }
        }
        return dp;
    }

}
