package main.algorithem;

public class FrogRiverOne {
    public int solution(int x, int[] a) {
        boolean[] isLeafAtPosition = new boolean[x+1];
        int numPositionsCovered = 0;
        for (int i = 0; i < a.length; i++) {
            if (!isLeafAtPosition[a[i]]) {
                isLeafAtPosition[a[i]] = true;
                numPositionsCovered++;
                if (numPositionsCovered == x) {
                    return i;
                }
            }
        }
        return -1;
    }

}
