package main.java.datastructures;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class BitSetQ {

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        List<Integer> sizeList = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                .map(Integer::parseInt)
                .collect(toList());

        BitSet b1 = new BitSet(sizeList.get(0));
        BitSet b2 = new BitSet(sizeList.get(0));
        Map<Integer, BitSet> bitSetMap = new HashMap<>();
        bitSetMap.put(1, b1);
        bitSetMap.put(2, b2);

        IntStream.range(0, sizeList.get(1)).forEach(i -> {
            try {
                List<String> operationValues = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" ")).collect(toList());
                String operation = operationValues.get(0);
                int integerOne = Integer.parseInt(operationValues.get(1));
                int integerTwo = Integer.parseInt(operationValues.get(2));
                if(operation.equalsIgnoreCase("AND")){
                    bitSetMap.get(integerOne).and(bitSetMap.get(integerTwo));
                }
                if(operation.equalsIgnoreCase("OR")){
                    bitSetMap.get(integerOne).or(bitSetMap.get(integerTwo));
                }
                if(operation.equalsIgnoreCase("XOR")){
                    bitSetMap.get(integerOne).xor(bitSetMap.get(integerTwo));
                }
                if(operation.equalsIgnoreCase("FLIP")){
                    bitSetMap.get(integerOne).flip(integerTwo);
                }
                if(operation.equalsIgnoreCase("SET")){
                    bitSetMap.get(integerOne).set(integerTwo);
                }

                System.out.printf("%d %d\n", b1.cardinality(), b2.cardinality());


            } catch (IOException e) {
                e.printStackTrace();
            }
        });


        bufferedReader.close();
    }
}
